

## TulipsTechnology Test - Santosh Ghimire

This app is develope Using docker, laravel 10 and php 8.1 +, this test app constist following feature

- [php 8.1 + eature](https://laravel.com/).
- frontend technology - vuejs
- Vuex for maintain state and authentication
- Laravel Passport for Authentication and Authorization at backend
- backend js - laravel 10.8 
- design patterns - [ service pattern and pipeline pattern ]
- covered all possible test cases
- docker implementation


## Main Branch
- main branch of this project is ,main branch 
- all merge request are merge into main branch 

## Installation and setup

- You must have docker, or Docker Desktop 
- if dont have docker download docker from (https://www.docker.com/products/docker-desktop/)
- Dont Forgot to Run php artisan passport:install after migrate refresh or fresh command , this will generate a key on database

## Install PHP , MYSQL, NGINX...

- We have docker compose file  that define a image and container

```
$ cp .env.example .env
$ docker compose build (only first time or if any settings have changed)
$ docker compose up -d
$ docker compose exec app composer install
$ docker compose exec app npm install
$ docker compose exec app php artisan key:generate
$ docker compose exec app php artisan migrate --seed
$ docker compose exec app php artisan passport:install
$ docker compose exec app php artisan storage:link
```

# what if user permission denied issue
```angular2html
$ sudo chmod -R 777 /search-task or ./ 
    OR
    sudo chown command as per need
```

# Run Test Check
```angular2html
$ docker compose exec app php artisan test

```


# Run Laravel App Locally
- Hit  this Url on browser (http://localhost:9000/)
- Goto App bash docker container
```angular2html
$ docker compose exec app sh
```
# Daatbase  Username and password
```angular2html
$ username : root
    password : root
    database : search-task

```

# Run   Mysql Adminiuer locally
- Hit  this Url on browser http://localhost:9001 password and user is root
- Goto App bash docker container
