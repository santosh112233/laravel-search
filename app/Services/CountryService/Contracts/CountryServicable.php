<?php

namespace App\Services\CountryService\Contracts;


use App\Models\Country;
use App\Services\CountryService\DTO\CountryRequestData;
use Illuminate\Support\Collection;

interface CountryServicable
{
    public function store(CountryRequestData $pageRequestData): Country;

    public function getCountries(): Collection;

    public function getCountry(int $id): Country;
}
