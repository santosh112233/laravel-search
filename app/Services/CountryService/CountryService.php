<?php

namespace App\Services\CountryService;

use App\DTO\CountryRequestData;
use App\Models\Country;
use App\Services\CountryService\Contracts\CountryServicable;
use Illuminate\Support\Collection;

final class CountryService implements CountryServicable
{
    public function __construct(
        public Country $country
    ){

    }

    public function getCountries(): Collection
    {
        return $this->country->get();
    }

    public function store($pageRequestData): Country
    {
        return $this->country->create([
            'country_name' => $pageRequestData->country_name,
        ]);
    }

    public function getCountry(int $id): Country
    {
        return $this->country->findOrFail($id);
    }

    public function deleteCountry(int $id): bool
    {
        return $this->country->findOrFail($id)->delete();
    }
}
