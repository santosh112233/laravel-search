<?php

namespace App\Services\CountryService\DTO;

class CountryRequestData
{
    public  function  __construct(
        public readonly string $country_name,
        public readonly ?string $country_code,
    ){

    }
}
