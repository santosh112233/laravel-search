<?php

namespace App\Services\PopulationService\DTO;

class PopulationRequestData
{
    public  function  __construct(
        public readonly int $country_id,
        public readonly int $city_id,
        public readonly int $age_group_id,
        public readonly int $male_population,
        public readonly int $female_population,

    ){

    }
}
