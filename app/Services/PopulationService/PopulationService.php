<?php

namespace App\Services\PopulationService;


use App\Models\Population;
use App\Services\ImageService\ImageServiceFacade;
use App\Services\PopulationService\Contracts\PopulationServicable;
use App\Services\PopulationService\Pipelines\QueryFilters\CityFilter;
use App\Services\PopulationService\Pipelines\QueryFilters\CountryFilter;
use Illuminate\Pipeline\Pipeline;
use Illuminate\Support\Collection;

final class PopulationService implements PopulationServicable
{
    public function __construct(
        public Population $population
    )
    {

    }

    public function getPopulations($request = null, $paginatePerPage = null): Collection
    {

        $populations = app(Pipeline::class)
            ->send($this->population->query())
            ->through([
                CountryFilter::class,
                CityFilter::class,
            ])
            ->thenReturn() ;
        return !is_null($paginatePerPage) ? $populations->with('ageGroup', 'country','city')->paginate($paginatePerPage) : $populations->with('ageGroup', 'country','city')->get();
    }

    public function store($pageRequestData): Population
    {
        return $this->population->create([
            'country_id' => $pageRequestData->country_id,
            'city_id' => $pageRequestData->city_id,
            'male_population' => $pageRequestData->male_population,
            'female_population' => $pageRequestData->female_population,
            'age_group_id' => $pageRequestData->age_group_id,

        ]);
    }

    public function getPopulation(int $id): Population
    {
        return $this->population->findOrFail($id);
    }

    public function deletePopulation(int $id): bool
    {
        return $this->population->findOrFail($id)->delete();
    }
}
