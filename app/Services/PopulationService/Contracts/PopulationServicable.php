<?php

namespace App\Services\PopulationService\Contracts;

use App\Models\Page;
use App\Models\Population;
use App\Services\PopulationService\DTO\PopulationRequestData;
use Illuminate\Support\Collection;

interface PopulationServicable
{
    public function store(PopulationRequestData $pageRequestData): Population;

    public function getPopulations($request = null, $paginatePerPage = null): Collection;

    public function getPopulation(int $id): Population;
}
