<?php

namespace App\Services\PopulationService\Pipelines\QueryFilters ;

use Closure;

class CountryFilter
{
    public function handle($request, Closure $next, ...$remove)
    {
        if (!request()->has('country')) {
            return $next($request);
        }
        return $next($request)->where( 'country_id', request()->country );

    }
}
