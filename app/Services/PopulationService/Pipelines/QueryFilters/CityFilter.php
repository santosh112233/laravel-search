<?php

namespace App\Services\PopulationService\Pipelines\QueryFilters ;

use Closure;

class CityFilter
{
    public function handle($request, Closure $next, ...$remove)
    {
        if (!request()->has('city')) {
            return $next($request);
        }
        return $next($request)->where( 'city_id', request()->city );

    }
}
