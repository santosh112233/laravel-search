<?php

namespace App\Services\CityService;

use App\Models\City;
use App\Models\Page;
use App\Services\CityService\Contracts\CityServicable;
use Illuminate\Support\Collection;

final class CityService implements CityServicable
{
    public function __construct(
        public  City $city
    )
    {

    }

    public function getCities(): Collection
    {
        return $this->city->get();
    }

    public function store($pageRequestData): City
    {

        return $this->city->create([
            'city_name' => $pageRequestData->city_name,
            'country_id' => $pageRequestData->country_id,

        ]);
    }

    public function getCity(int $id): City
    {
        return $this->city->findOrFail($id);
    }

    public function deleteCity(int $id): bool
    {
        return $this->city->findOrFail($id)->delete();
    }
}
