<?php

namespace App\Services\CityService\Contracts;

use App\Models\City;
use App\Services\CityService\DTO\CityRequestData;
use Illuminate\Support\Collection;

interface CityServicable
{
    public function store(CityRequestData $pageRequestData): City;

    public function getCities(): Collection;

    public function getCity(int $id): City;
}
