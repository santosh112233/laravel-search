<?php

namespace App\Services\CityService\DTO;

class CityRequestData
{
    public  function  __construct(
        public readonly ?string $city_code,
        public readonly string $city_name,
        public readonly int $country_id,

    ){

    }
}
