<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

final class FrontendController extends Controller
{
    public function index()
    {
       return view('frontend.index');
    }
}
