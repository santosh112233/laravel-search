<?php

namespace App\Http\Controllers;

use App\Http\Resources\AgeGroupResource;
use App\Models\AgeGroup;
use Exception;
use Illuminate\Http\Request;

final class AgeGroupController extends Controller
{
    public function __construct(
        public AgeGroup $ageGroup
    ){
        $this->middleware('auth:api', ['except' => [
            'index'
        ]]);
    }

    /**
     * this  api is only for read only, no CRUD are required
     */
    public function index()
    {
        try{
            return AgeGroupResource::collection($this->ageGroup->get());
        }catch(Exception $e){
            return response(['error' => 'sometihng wennt wrong']);
        }
    }
}
