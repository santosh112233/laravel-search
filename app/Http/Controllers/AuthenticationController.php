<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginDataStore;
use Exception;
use Illuminate\Http\Request;

class AuthenticationController extends Controller
{
    /**
     * this member function with make login and response token and user detail
     */
    public function login(LoginDataStore $request)
    {
        try{
            if (!auth()->attempt($request->all())) {
                return response(['error_message' => 'Incorrect Details.
                Please try again']);
            }
            $token = auth()->user()->createToken('API Token')->accessToken;
        }catch(Exception $e){
            return response(['error_message' => $e->getMessage()]);
        }
        return response(['user' => auth()->user(), 'token' => $token]);

    }
}
