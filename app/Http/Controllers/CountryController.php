<?php

namespace App\Http\Controllers;

use App\Http\Requests\CountryStoreRequest;
use App\Http\Resources\CountryResource;

use App\Services\CountryService\Contracts\CountryServicable;
use Exception;
use Illuminate\Http\Request;
use DB;

final class CountryController extends Controller
{
    public function __construct(
        public CountryServicable $countryServicable
    ){
        $this->middleware('auth:api', ['except' => [
            'index'
        ]]);
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try{
            $country = $this->countryServicable->getCountries();
            return CountryResource::collection( $this->countryServicable->getCountries());
        }catch(Exception $e){
            return response(['error' => 'something went wrong']);
        }

    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(CountryStoreRequest $request)
    {
        try{
            DB::beginTransaction();
            $country = $this->countryServicable->store($request->toDto());
        }catch(Exception $e){
            DB::rollBack();
            return response(['error' => 'something wend wrong while saving']);

        }
        DB::commit();
        return new CountryResource($country);
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try{
            DB::beginTransaction();
            $this->countryServicable->deleteCountry($id);
        }catch(Exception $e){
            DB::rollBack();
            return response(['error' => 'something wend wrong while delete']);
        }
        DB::commit();
        return response(204);
    }
}
