<?php

namespace App\Http\Controllers;

use App\DataFactory\PopulationDataFactory;
use App\Http\Requests\PopulationStoreRequest;
use App\Http\Resources\PopulationResource;
use App\Services\PopulationService\Contracts\PopulationServicable;
use App\Services\PopulationService\PopulationService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use DB;

final class PopulationController extends Controller
{

    public function __construct(
        public PopulationServicable $populationServicable
    ){
        $this->middleware('auth:api', ['except' => [
            'index', 'populationSummary', 'popuationSummaryAgeWise'
        ]]);
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        try{
            $populations = $this->populationServicable->getPopulations($request);
        }catch(Exception $e){
            return response(['error' => 'something went wrong']);
        }
        return response(['data' => $populations], 200);

    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(PopulationStoreRequest $request)
    {
        try{
            DB::beginTransaction();
            $populations = $this->populationServicable->store($request->toDto());
        }catch(Exception $e){
            DB::rollBack();
            return response(['error' => 'something went wrong']);
        }
        DB::commit();
        return new PopulationResource($populations);
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try{
            DB::beginTransaction();
            $this->populationServicable->deletePopulation($id);
        }catch(Exception $e){
            DB::rollBack();
            return response(['error' => 'something went wrong']);
        }
        DB::commit();
        return response(204);

    }
    /**
     * this method return the population country wise
     */
    public function populationSummary()
    {
        try{
            DB::beginTransaction();
            $populations =PopulationDataFactory::make($this->populationServicable->getPopulations(null));
            $newPop = $populations->groupBy('country_name');
        }catch(Exception $e){
            return response(['error' => 'something went wrong']);
        }
        DB::commit();
        return response(['data' => $newPop], 200);
    }
    /**
     * this method return the population age group wise
     */
    public function popuationSummaryAgeWise(Request $request)
    {
        try{
            $populations =PopulationDataFactory::make($this->populationServicable->getPopulations($request) );
            $newPop = $populations->groupBy('age_group');
        }catch(Exception $e){
            return response(['error' => 'something went wrong']);
        }
        return response(['data' => $newPop], 200);

    }
}
