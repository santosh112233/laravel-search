<?php

namespace App\Http\Controllers;

use App\Http\Requests\CityStoreRequest;
use App\Http\Requests\CountryStoreRequest;
use App\Http\Resources\CityResource;
use App\Http\Resources\CountryResource;
use App\Services\CityService\Contracts\CityServicable;
use Exception;
use Illuminate\Http\Request;

final class CityController extends Controller
{
    public function __construct(
            public CityServicable $cityServicable
        ){
            $this->middleware('auth:api', ['except' => [
                'index'
            ]]);
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        try{
            $city = $this->cityServicable->getCities();

        }catch(Exception $e){
            return response(['error' => 'something went wrong']);
        }
        return CityResource::collection($city);

    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(CityStoreRequest $request)
    {
        try{
            return new CityResource($this->cityServicable->store($request->toDto()));
        }catch(Exception $e){
            return response(['error' => 'something went wrong']);
        }

    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try{
            $this->cityServicable->deleteCity($id);
        }catch(Exception $e){
            return response(['error' => 'something went wrong while delete']);
        }
        return response(204);
    }
}
