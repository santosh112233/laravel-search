<?php

namespace App\Http\Requests;

use App\Services\PopulationService\DTO\PopulationRequestData;
use Illuminate\Foundation\Http\FormRequest;

class PopulationStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'country_id' => 'required',
            'city_id' => 'required',
            'age_group_id' => 'required',
            'male_population' => 'required',
            'female_population' => 'required',
        ];
    }

    public function toDto(): PopulationRequestData
    {
        return new PopulationRequestData(
            country_id : $this->country_id,
            city_id : $this->city_id,
            age_group_id : $this->age_group_id,
            male_population : $this->male_population,
            female_population : $this->female_population
        );
    }
}
