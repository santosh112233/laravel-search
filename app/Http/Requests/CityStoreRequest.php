<?php

namespace App\Http\Requests;

use App\Services\CityService\DTO\CityRequestData;
use Illuminate\Foundation\Http\FormRequest;

class CityStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'city_name' => 'required',
            'country_id' => 'required',
        ];

    }

    public function toDto(): CityRequestData
    {
        return new CityRequestData(
            city_code: $this->city_code,
            city_name : $this->city_name,
            country_id: $this->country_id
        );
    }
}
