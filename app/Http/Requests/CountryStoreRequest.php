<?php

namespace App\Http\Requests;

use App\Services\CountryService\DTO\CountryRequestData;
use Illuminate\Foundation\Http\FormRequest;

class CountryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'country_name' => 'required',
        ];
    }

    public function toDto(): CountryRequestData
    {
        return new CountryRequestData(
            country_name : $this->country_name,
            country_code : $this->country_code
        );
    }
}
