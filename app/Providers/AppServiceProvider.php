<?php

namespace App\Providers;

use App\Services\CityService\CityService;
use App\Services\CityService\Contracts\CityServicable;
use App\Services\CountryService\Contracts\CountryServicable;
use App\Services\CountryService\CountryService;
use App\Services\PopulationService\Contracts\PopulationServicable;
use App\Services\PopulationService\PopulationService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(CountryServicable::class, CountryService::class);
        $this->app->bind(CityServicable::class, CityService::class);
        $this->app->bind(PopulationServicable::class, PopulationService::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
