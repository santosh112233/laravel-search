<?php

namespace App\DataFactory;
use Illuminate\Support\Collection;

class PopulationDataFactory
{
    public static function make($populationData): Collection
    {

    return $populationData->map(function($item){
        return [
            'male' => $item->male_population,
            'female' => $item->female_population,
            'age_group' => $item->ageGroup->age_group,
            'country_name' => $item->country->country_name,
            'city_name' => $item->city->city_name,
        ];
    });

    }
}
