<?php

namespace Tests\Feature;

use App\Models\AgeGroup;
use App\Models\City;
use App\Models\Country;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use JsonException;

class PapulationControllerTest extends TestCase
{
    /**
     * @dataProvider validDataProvider
     * @throws \JsonException
     */
    public function testCanCreatePopulationWithValidData(array $data): void
    {
        $user = $this->loginAsAdmin();
        $this->actingAs($user, 'api');

        $city = City::factory()->create();
        $country = Country::factory()->create();
        $age =  AgeGroup::factory()->create();

        $data['city_id'] = $city->id ;
        $data['country_id'] = $country->id ;
        $data['age_group_id'] = $age->id ;

        $response = $this->post(route('populations.store'), $data);

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('populations', [
            'male_population' => $data['male_population'],
        ]);
    }

    /**
     * @dataProvider inValidDataProvider
     * @throws \JsonException
     */
    public function testCanNotCreatePopulationWithInvalidData(array $data): void
    {
        $user = $this->loginAsAdmin();
        $this->actingAs($user, 'api');

        $response = $this->post(route('populations.store'), $data);

        $response->assertSessionHasErrors();
        $this->assertDatabaseMissing('populations', [
            'male_population' => $data['male_population'],
            'female_population' => $data['female_population'],
            'country_id' => $data['country_id'],
            'city_id' => $data['country_id'],

        ]);
    }

    protected function validDataProvider(): array
    {

        return [
            [
                [
                    'male_population' => '100000',
                    'female_population' => '100000',
                ],
                [
                    'male_population' => 360,
                    'female_population' => 350,
                ],
                [
                    'city_id' => 1,
                    'country_id' => 1,
                    'age_group_id' => 1,
                    'male_population' => 360,
                    'female_population' => 350,
                ]
            ]
        ];
    }

    protected function inValidDataProvider(): array
    {
        return [
            [
                [
                    'city_id' => 'xxxx',
                    'country_id' => '',
                    'age_group_id' => 1,
                    'male_population' => '100000',
                    'female_population' => '100000',
                ],
                [
                    'city_id' => '',
                    'country_id' => '',
                    'age_group_id' => 1,
                    'male_population' => '100000',
                    'female_population' => '100000',
                ],
            ]
        ];
    }
}
