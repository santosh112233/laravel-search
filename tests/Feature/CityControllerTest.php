<?php

namespace Tests\Feature;

use App\Models\Country;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use JsonException;

class CityControllerTest extends TestCase
{
    /**
     * @dataProvider validDataProvider
     * @throws JsonException
     */
    public function testUserCanCreateCityWithValidData(array $data): void
    {
        $user = $this->loginAsAdmin();
        // $this->actingAs($user);
        $this->actingAs($user, 'api');

        $response = $this->post(route('cities.store'), $data);

        $response->dumpHeaders();

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('cities', [
            'city_name' => $data['city_name'],
        ]);
    }

    /**
     * @dataProvider inValidDataProvider
     * @throws \JsonException
     */
    public function testUserCanNotCreateCityWithInvalidData(array $data): void
    {
        $user = $this->loginAsAdmin();
        $this->actingAs($user, 'api');

        $response = $this->post(route('cities.store'), $data);

        $response->assertSessionHasErrors();
        $this->assertDatabaseMissing('cities', [
            'city_name' => $data['city_name'],
            'country_id' => $data['country_id']
        ]);
    }

    protected function validDataProvider(): array
    {
        $country = Country::factory(1)->create();
        return [
            [
                [
                    'city_name' => 'demo city',
                    'country_id' => $country->id
                ]
            ]
        ];
    }

    protected function inValidDataProvider(): array
    {
        return [
            [
                [
                    'city_name' => 'xxxx',
                    'country_id' => ''
                ],
                [
                    'city_name' => '',
                    'country_id' => ''
                ],
            ]
        ];
    }
}
