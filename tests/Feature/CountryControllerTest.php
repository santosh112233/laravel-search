<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use JsonException;
use Tests\TestCase;

class CountryControllerTest extends TestCase
{
    /**
     * @dataProvider validDataProvider
     * @throws JsonException
     */
    public function testUserCanCreateCountryWithValidData(array $data): void
    {
        $user = $this->loginAsAdmin();
        $this->actingAs($user, 'api');

        $response = $this->post(route('countries.store'), $data);

        $response->assertSessionHasNoErrors();
        $this->assertDatabaseHas('countries', [
            'country_name' => $data['country_name'],
        ]);
    }

    /**
     * @dataProvider inValidDataProvider
     * @throws JsonException
     */
    public function testUserCanNotCreateCountryWithInvalidData(array $data): void
    {
        $user = $this->loginAsAdmin();
        $this->actingAs($user, 'api');

        $response = $this->post(route('countries.store'), $data);

        $response->assertSessionHasErrors();
        $this->assertDatabaseMissing('countries', [
            'country_name' => $data['country_name'],
        ]);
    }

    protected function validDataProvider(): array
    {
        return [
            [
                [
                    'country_name' => 'null'

                ]
            ]
        ];
    }

    protected function inValidDataProvider(): array
    {
        return [
            [
                [
                    'country_name' => '',
                ],

            ]
        ];
    }
}
