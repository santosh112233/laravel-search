<?php

use App\Http\Controllers\AgeGroupController;
use App\Http\Controllers\AuthenticationController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\PopulationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::middleware(['api'])->group(function () {
    Route::resources([
        'countries' =>  CountryController::class,
        'populations' => PopulationController::class,
        'cities' => CityController::class,
        'agegroups' => AgeGroupController::class,
    ] );

    Route::get('/populations/summary/country/data', [PopulationController::class, 'populationSummary' ]);
    Route::get('/populations/summary/agewise/data', [PopulationController::class, 'popuationSummaryAgeWise' ]);
    Route::post('/login', [AuthenticationController::class, 'login'])->name('login');
});



