require('./bootstrap');

import frontendview from "./Components/Frontend.vue";
import countryview from "./Components/Country/Index.vue";
import cityview from "./Components/City/Index.vue";
import populationview from "./Components/Population/Index.vue";
import loginview from "./Components/Auth/Login.vue";
import { createApp } from 'vue'
import BootstrapVue3 from 'bootstrap-vue-3'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue-3/dist/bootstrap-vue-3.css'
import Store from "./Store";




import {createRouter, createWebHistory} from "vue-router";

const routes = [
    // {
    //     path: "/page-detail/:id",
    //     name: "PageDetail",
    //     component: PageView,
    // },
    {
        path: "/",
        name: "main-view",
        component: frontendview,
    },
    {
        path: "/country",
        name: "country-page",
        component: countryview,
    },
    {
        path: "/city",
        name: "city-page",
        component: cityview,
    },
    {
        path: "/population",
        name: "population-page",
        component: populationview,
    },
    {
        path: "/login",
        name: "login-page",
        component: loginview,
    },
]
const router = createRouter({
    // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
    history: createWebHistory(process.env.APP_URL),
    routes, // short for `routes: routes`
})


const app = createApp({
    components: {
        frontendview
    },

})

app.use(BootstrapVue3)
    .use(router)
    .use(Store)
    .mount('#app');




