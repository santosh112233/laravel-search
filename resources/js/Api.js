export  const  AssetUrl = "http://localhost:9000/"

export const Country = "/api/countries";
export const City = "/api/cities";
export const Population = "/api/populations/";
export const PopulationSummary = "/api/populations/summary/country/data";
export const PopulationSummaryAgewise = "/api/populations/summary/agewise/data";
export const AgeGroup = "/api/agegroups";

//  login apis

export const Login = "/api/login";

export const Loggout = "/api/logout";




