import axios from "axios"
import { Login } from "../Api"
// import { attempt } from "lodash"

export default {
    namespaced : true,
    state : {
        token : '',
        user :''

    },

    getters : {

        authenticated (state){
            return state.token
        }

    },

    mutations : {
        SET_TOKEN (state, token){
            state.token = token
        },
    },

    actions : {

        async Login({ dispatch } , loginData){

            let response = await axios
                .post(Login, loginData, {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    }
                })

            return dispatch('attempt', response.data.token)

        },

        async attempt({ commit}, token){
            console.log(token);
            commit('SET_TOKEN', token)
            localStorage.setItem('token', token);

        }

    },

}
