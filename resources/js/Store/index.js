// import Vue from 'vue';
import { createApp } from 'vue'
import Vuex from 'vuex';
import auth from './auth';

const app = createApp();
app.use(Vuex);

export default new Vuex.Store({
    state : {

    },

    actions : {

    },

    modules : {
        auth

    },
    mutations : {

    }

})
