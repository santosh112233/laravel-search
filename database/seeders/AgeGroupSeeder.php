<?php

namespace Database\Seeders;

use App\Models\AgeGroup;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AgeGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $ageGroups = ['Old', 'Young', 'Child'];
        foreach($ageGroups as $ageGroup){
            AgeGroup::insert([
                'age_group' => $ageGroup
            ]);
        }
    }
}
