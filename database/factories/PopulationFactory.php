<?php

namespace Database\Factories;

use App\Models\AgeGroup;
use App\Models\City;
use App\Models\Country;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Population>
 */
class PopulationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'country_id' => function () {
                return Country::factory()->create()->id;
            },
            'city_id' => function () {
                return City::factory()->create()->id;
            },
            // 'age_group_id' => function () {
            //     return AgeGroup::factory()->create()->id;
            // },
            'age_group_id' => $this->faker->numberBetween(1,3),
            'male_population' => $this->faker->numberBetween(100000-1500000),
            'female_population' => $this->faker->numberBetween(100000-1500000),
        ];
    }
}
